# Exercício prático para a P1

Prof. Victor Borba
-----------------
Fernando Silva Maransatto

fernando.maransatto@gmail.com

# Exercício 1

![](exercicio-pratico-p1.png)

### 1. Representação matemática do grafo G:

    V(G) = {a,b,c,d,e}
    E(G) = {
        (a,c),
        (a,d),
        (b,c),
        (b,e),
        (b,d),
        (e,d)
    }

### 2. Qual o grau do vértice d?

    A quantidade de graus do vértice d é 3:
    {(a,d), (b,d), (e,d)}

### 3. Qual a ordem do grafo G?

    5

### 4. Construa um grafo H complemento do grafo G

<span style="color:red"> Este exercício está errado. Na verdade o complemento é o que falta para o grafo G ser completo.</span>

![image](grafo-h.png)

### 5. Crie a matriz de adjacências do grafo G

![](matriz-adjacencia-grafo-G.png)

### 6. Crie a lista de adjacências do grafo H

    a -> b,c,d,e
    b -> a,c,d,e
    c -> a,b,d,e
    d -> a,b,c,e
    e -> a,b,c,d

### 7. Faça a representação matemática do grafo J

### 8. Qual o grau do vértice d e do vértice b?

### 9. Qual a ordem do grafo J?

### 10. Crie a matriz e a lista de adjacências do grafo J