grafo = [
    [1,2,3,4],
    [2,1,3],
    [3,1,2],
    [4,1,5],
    [5,4],
    [6,7],
    [7,6,8],
    [8,7],
    [9,10],
    [10,9]
]

componentes = []
visitados = []

def buscaAdjacentes(vertice):

    adjacentes = []
    for a in range(1, len(vertice)):
        adjacentes.append(vertice[a])

    return adjacentes

def buscaEmProfundidade(vertice_inicial):

    if vertice_inicial not in visitados:
        visitados.append(vertice_inicial)

    for v in grafo:
        if v[0] == vertice_inicial:
            adjacentes = buscaAdjacentes(v)

            # Busca em profundidade do primeiro adjacente
            for a in adjacentes:
                if a not in visitados:
                    visitados.append(a)
                    buscaEmProfundidade(a)


contador_componente = 0

# A lógica da contagem de componentes está aqui:
for vertice in grafo:

    for adjacente in vertice:
        if adjacente not in visitados:
            visitados = []
            buscaEmProfundidade(adjacente)
            componentes.append(visitados)

print(componentes)
print('total de componentes: ' + str(len(componentes)))
