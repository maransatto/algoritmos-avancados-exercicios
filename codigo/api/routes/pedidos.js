const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).json({
        mensagem: 'Retorna os pedidos'
    });
});

router.post('/', (req, res, next) => {
    res.status(201).json({
        mensagem: 'O pedido foi criado'
    });
});
router.get('/:id_pedido', (req, res, next) => {
    res.status(200).json({
        mensagem: 'Detalhes do Pedido',
        id_pedido: req.params.id_pedido
    });
});

router.delete('/', (req, res, next) => {
    res.status(200).json({
        mensagem: 'Pedido excluído'
    });
});

module.exports = router;