const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;

// RETORNA TODOS OS PRODUTOS
router.get('/', (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query('select * from produtos;', (error, results, filds) => {
            conn.release();

            if (error) {
                res.status(500).json({ error: error, response: null });
            } else {
                res.status(200).json({ error: null,response: results });
            }
        });
    });
});

// INCLUSÃO DE UM PRODUTO
router.post('/', (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            'insert into produtos (nome, preco) values (?, ?);',
            [req.body.nome, req.body.preco],
            (error, results, fields) => {
                conn.release();

                if (error) {
                    res.status(500).json({
                        error: error,
                        response: null
                    });
                } else {
                    res.status(201).json({
                        mensagem: 'Produto inserido com sucesso',
                        id: results.insertId
                    });
                }
        });
    });

});

// RETORNANDO UM PRODUTO POR ID
router.get('/:id_produto', (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(`select * from produtos where id_produto = ${ req.params.id_produto };`, (error, results, filds) => {
            conn.release();

            if (error) {
                res.status(500).json({ error: error, response: null });
            } else {
                res.status(200).json({ error: null,response: results });
            }
        });
    });
});

// ALTERAR UM PRODUTO
router.patch('/', (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `update produtos set nome = ?, preco = ? where id_produto = ?;`,
            [req.body.nome, req.body.preco, req.body.id_produto],
            (error, results, fields) => {
                conn.release();

                if (error) {
                    res.status(500).json({
                        error: error,
                        response: null
                    });
                } else {
                    res.status(201).json({
                        mensagem: 'Produto atualizado com sucesso'
                    });
                }
        });
    });
});

// REMOVER UM PRODUTO
router.delete('/', (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `delete from produtos where id_produto = ?;`,
            [req.body.id_produto],
            (error, results, fields) => {
                conn.release();

                if (error) {
                    res.status(500).json({
                        error: error,
                        response: null
                    });
                } else {
                    res.status(201).json({
                        mensagem: 'Produto removido com sucesso'
                    });
                }
        });
    });
});

module.exports = router;