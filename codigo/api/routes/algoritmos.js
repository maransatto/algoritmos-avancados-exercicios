const express = require('express');
const router = express.Router();

/**
 * Função recursiva que retorna o resultado de um número multiplicado por outro
 */
function produto(x, y) {
    if (y > 1) {
        return x + produto(x, y-1);
    } else {
        return x;
    }
}

/**
 * Método de POST para retornar o produto usando função recursiva
 */
router.post('/produto', (req, res, next) => {

    x = req.body.x;
    y = req.body.y;

    res.status(200).json({
        resultado: 'O produto de ' + x + ' e ' + y + ' é ' + produto(x, y)
    });
});

/**
 * Função recursiva que retorna um número elevado à uma potência
 * @param {número} x 
 * @param {potência} y 
 */
function potencia(x, y) {
    if (y > 1) {
        return produto(x, potencia(x, y-1));
    } else {
        return x;
    }
}

/**
 * Método de POST para retornar a potência usando função recursiva
 */
router.post('/potencia', (req, res, next) => {

    x = req.body.x;
    y = req.body.y;

    res.status(200).json({
        resultado: 'A potência de ' + x + ' elevado a ' + y + ' é ' + potencia(x, y)
    });
});

/**
 * Função recursiva que retorna a soma de todos os números até o número informado
 * @param {número} n 
 */
function soma(n) {
    if (n > 0) {
        return n + soma(n-1);
    } else {
        return 0;
    }
}

/**
 * Método de POST para retornar a soma recursiva de todos os números até o número informado
 */
router.post('/soma', (req, res, next) => {

    n = req.body.n;

    res.status(200).json({
        resultado: 'A soma de todos os números até ' + n + ' é: ' + soma(n)
    });
});

/**
 * Retorna o primeiro algarismo de um número qualquer
 * @param {n} n Número inicial
 */
function primeiroAlgarismo(n) {
    
    n = parseInt(n / 10);
    console.log(n);
    if (n <= 9) {
        return n;
    } else {
        return primeiroAlgarismo(n);
    }
}

/**
 * Método de POST para retornar o primeiro algarismo de um número qualquer
 */
router.post('/primeiro_algarismo', (req, res, next) => {

    n = req.body.n;

    res.status(200).json({
        resultado: 'O primeiro algarismo de ' + n + ' é: ' + primeiroAlgarismo(n)
        // resultado: 'O primeiro algarismo de ' + n + ' é: ' + parseInt(n / 10)
    });
});

/**
 * Remove ítem da lista
 * @param {lista} lista Lista de Números
 * @param {i} i índice da lista de números
 * @param {n} n número que é pra apagar
 */
function apagaNumeroPecadorDibradorEHerege(lista, i, n) {
    if (i < 0) {
        return lista;
    } else {
        if (lista[i] === n) {
            lista.splice(i, 1);
        }
        return apagaNumeroPecadorDibradorEHerege(lista, i-1, n);
    }
}

/**
 * Método recursivo POST que permite remover um número de uma lista
 */
router.post('/remove_numero_pecador', (req, res, next) => {

    lista   = req.body.lista;
    n       = req.body.n;

    res.status(200).json({
        resultado: 'Lista dos números que não são pecadores, nem hereges, nem dibradores: ' + apagaNumeroPecadorDibradorEHerege(lista, lista.length-1, n)
    });

});

/**
 * Função recursiva que retorna se o número é ou não perfeito
 * @param {*} valor Valor do número a ser verificado
 * @param {*} indice Índice atual dos números que estão sendo verificados
 * @param {*} soma Total da soma, controle da recursividade, começa com 0
 */
function perfeitoEhSohDeus(valor, indice=1, soma=0) {
    
    if (valor == indice) {
        return (soma == valor);
    } else {
        // Somente soma os números divisores do valor
        if (valor % (indice) == 0) {
            soma = soma + indice;
        }
        return perfeitoEhSohDeus(valor, indice+1, soma);
    }
}

/**
 * Método POST que retorna se o número informado é perfeito (soma dos divisores = valor)
 */
router.post('/perfeito_soh_Deus', (req, res, next) => {

    valor = req.body.valor;

    str = ' NÃO';
    if (perfeitoEhSohDeus(valor)) {
        str = ''; 
    }
    res.status(200).json({

        resultado: 'O número ' + valor + str + ' é perfeito' 
    });

});


// Algoritmo Guloso ---------------------

// CANDIDATOS
const moedas_disponiveis = [0.01,0.25,0.10,1,0.5];

var troco_total         = 0.0;
var moedas_selecionadas = [];
var troco_acumulado     = 0.0;

// FUNÇÃO DE SELEÇÃO
function selecionarMoedas() {
    
    // ordena da maior moeda pra menor, este é o meu critério de seleção    
    for (const moeda in moedas_disponiveis.sort(function(a, b) {return b - a; })) {
        if (moedas_disponiveis.hasOwnProperty(moeda)) {
            const m = Number.parseFloat(moedas_disponiveis[moeda].toFixed(2));
        
            if (eViavel(m)) {
                troco_acumulado = Number.parseFloat(troco_acumulado.toFixed(2)) + m;
                moedas_selecionadas.push(m);
                return;
            }
        }
    }
}

// FUNÇÃO DE VIABILIDADE
function eViavel(moeda) {
    return ((moeda) <= troco_total - troco_acumulado);
}

// FUNÇÃO DE OBJETIVO
function naoAcumulouOTroco() {
    return troco_acumulado < troco_total;
}

// SOLUÇÃO
function calcularTrocoDetalhado(preco, entrada) {
    troco_total = entrada.toFixed(2) - preco.toFixed(2);

    while (naoAcumulouOTroco()) {   
        selecionarMoedas();
    }

    return moedas_selecionadas;
}

router.post('/devolve-troco', (req, res, next) =>{
    preco       = req.body.preco;
    entrada     = req.body.entrada;

    troco_total         = 0.0;
    moedas_selecionadas = [];
    troco_acumulado     = 0.0;

    res.status(200).json({
        troco_detalhado: calcularTrocoDetalhado(preco, entrada),
        total_de_troco: troco_acumulado.toFixed(2)
    });
});

// -------------------------------------------------

module.exports = router;