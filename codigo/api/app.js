const express = require('express');
const app = express();
const morgan = require('morgan');

const bodyParser = require('body-parser');

const rotaProdutos = require('./routes/produtos');
const rotaPedidos = require('./routes/pedidos');
const rotaAlgoritmos = require('./routes/algoritmos');
const rotaRedes = require('./routes/redes');

// Algumas configurações da API
app.use(bodyParser.urlencoded({extended: false})); // informa que aceita urlencoded]
app.use(bodyParser.json()); // informa que aceita json como parâmetro de entrada

app.use(morgan('dev'));

app.use('/produtos', rotaProdutos);
app.use('/pedidos', rotaPedidos);
app.use('/redes', rotaRedes);

app.use('/algoritmos', rotaAlgoritmos);

app.use((req, res, next) => {
    const erro = new Error('Não encontrado');
    erro.status = 404;
    next(erro);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        erro: {
            mensagem: error.message
        }
    });
});

module.exports = app;