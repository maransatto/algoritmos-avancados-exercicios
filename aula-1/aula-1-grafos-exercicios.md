# Aula 1 - Introdução a Grafos

Prof. Victor Borba
-----------------
Fernando Silva Maransatto

fernando.maransatto@gmail.com

## Exercício 1

![](aula-1-exercicio-1.png)

### 1. Representação matemática do grafo(a):

    V(a) = {v1,v2,v3,v4,v5}

    E(a) = {
                (v1,v2),
                (v1,v3),
                (v1,v3),
                (v1,v4),
                (v1,v5),
                (v2,v3),
                (v2,v5),
                (v3,v4),
                (v3,v5),
                (v4,v5)
        }

### 2. Ordem e número de arestas de cada grafo

    - |V(a)| = 5
    - |E(a)| = 10

    - |V(b)| = 8
    - |E(b)| = 13

    - |V(c)| = 4
    - |E(c)| = 6

### 3. Quais grafos são completos? justifique os que não são

    O grafo **c** é o único completo, pois todos os vértices se conectam entre si e não é um multigrafo

    O grafo a não conecta o V2 no V4, e também é um multigrafo (v1,v3)

### 4. Quais os grafos são simples? Justifique os que não são

    Os grafos **a** e **b** são simples, pois só são considerados completos quando todos os vértices são adjacentes entre si e não são multigrafos.

### 5. No grafo **(a)**, quais vértices são adjacentes a v3? E quais arestas são adjacentes a (v3,v5) ?

    Os vértices adjacentes a v3 são v1, v2, v4, e v5
    As arestas adjacentes a v3,v5 são (v1,v5), (v2,v5), (v4,v5), (v1,v3), (v2,v3), (v1,v3), (v4,v3)

### 6. Há grafos regulares?

    Sim, o c, por que todos os vértices possuem a mesma quantidade de graus.

### 7. Todo grafo completo é regular?

    Sim, pois além de todos os vértices se conectarem a todos os outros vértices, todos possuem a mesma quantidade de arestas (graus).

### 8. Quantas arestas possuem um grafo completo de 200 vértices?

    |E| = 200(200-1)/2 = 19,900


## Exercício 2

![](aula-1-exercicio-2.png)

### 1. Monte um grafo para representar o mapa ao lado

![](aula1-exercicio-2-grafo.png)

### 2. Qual é o número de vértices?

    10

### 3. Qual é o número de Arestas?

    9

### 4. Qual a ordem do Grafo?

    10

### 5. É completo?

    Não, pois ele não possui todos as as vértices adjacentes

### 6. É simples, ou multigrafo?

    É simples, não possui duas arestas para os mesmos vértices

## Perguntas de Interpretação

### 7. O que significa o grau de cada vértice?

    A quantidade de arestas do vértice.

### 8. O que o conceito de adjacência significa?

    Representa as ligações em comum entre um vértice ou uma aresta com os demais vértices ou arestas.

### 9. Monte a representação matemática do conjunto do grafo.

    V(G) = {
                araraquara,
                sao_carlos,
                rio_claro,
                limeira,
                leme,
                araras,
                americana,
                campinas,
                jundiai,
                sao_paulo
           }

    E(G) = {
                (araraquara, sao_carlos),
                (sao_carlos, rio_claro),
                (rio_claro, limeira),
                (leme, araras),
                (araras, limeira),
                (limeira, americana),
                (americana, campinas),
                (campinas, jundiai),
                (jundia, sao_paulo),
        }