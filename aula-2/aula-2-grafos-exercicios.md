# Aula 2 Grafos Orientados

Prof. Victor Borba
-----------------
Fernando Silva Maransatto

fernando.maransatto@gmail.com

## Exercício 1

![image](aula-2-exercicio-1.png)

### 1. O grafo (a) é regular?

    Sim, pois todos os vértices possuem a mesma quantidade de graus.

### 2. Existe alguma fonte ou sumidouro no grafo (b). Se houver, informe os graus de entrada e saída para ele.

    Fonte: D_in(v5) = 4
    Sumidouro: D_out(v4) = 3

### 3. No grafo (b), dê exemplo de três arestas e cite os vértices que elas são convergentes e divergentes.

    (v5,v2) é divergente a v5 e convergente a v2
    (v3,v4) é divergente a v3 e convergente a v4
    (v1,v4) é divergente a v1 e convergente a v4

### 4. Quantas arestas há no grafo (b) ?

    |E(b)| = 10 (quantidade de arestas do grafo)

### 5. Qual a ordem do grafo (b)

    |V(b)| = 5 (ordem do grafo)

### 6. Quais arestas são adjacentes à aresta (v2,v1)?

    (v2,v3), (v5,v2), (v1,v3), (v5,v1), (v1,v3), (v1,v4)

### 7. Faça a representação matemática de conjunto para o grafo (b)

    D = {V,E}
    V(D) = {v1,v2,v3,v4,v5}
    E(D) = {
                (v1,v3),
                (v1,v3),
                (v1,v4),
                (v2,v1),
                (v2,v3),
                (v3,v4),
                (v5,v2),
                (v5,v1),
                (v5,v4),
                (v5,v3)
           }

# Exercício 2

### 1. Faça uma figura de um caminho de comprimento 0, de um caminho de comprimento 1 de um caminho de comprimento 2. Faça uma figura de um ciclo de comprimento 3 e de um ciclo de comprimento 4.

![image](aula-2-exercicio-2-1.png)

### 2. Se V o conjunto {a,b,c,d,e} e E o conjunto de {de, bc, ca, be}. Verifique que o grafo (V,E) é um caminho. Agora suponha que F é o conjunto {bc, bd, ea, ed, ac} e verifique que o grafo (V,F) é um ciclo.

![image](aula-2-exercicio-2-2.png)

### 3. Se G é um Kn, quanto valem o seu grau mínimo e o seu grau máximo?

    O mínimo é sempre igual ao máximo pois todos os vértices são adjacentes, já que Kn representa grafo completo.