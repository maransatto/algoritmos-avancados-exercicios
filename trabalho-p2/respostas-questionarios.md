# Algoritmos Avançados - Questionários - P2

## Grupo 1 - Algoritmo dos mínimos sucessivos

### 1. Aponte as características do algoritmo dos mínimos sucessíveis.

*Resposta:*

- O vértice de partida pode ser aleatório;
- O próximo precisa ser o de menor peso;
- Forma um ciclo hamiltoniano;
- Todos os vértices só podem ser visitados 1 vez;
- Todos os vértices precisam ser o ponto de partida para que formem ciclos hamiltonianos;
- Depois de formar todos os ciclos, somam-se as arestas e o resultado é o ciclo de menor peso;
- Ideal para o problema de disponibilidade dos professores para as matérias da turma.

### 2. Qual é o algoritmo mais apropriado para resolver o problema? Busca em largura ou busca em profundidade? Por que?

    Profundidade, por que não pode repetir vértice

*Resposta:*

### 3. Com base no grafo a seguir, encontre o menor caminho possível, de acordo com a lógica dos mínimos sucessíveis.

**A**) 2 + 1 + 9 + 8 + 6 = 26

**B**) 1 + 3 + 4 + 8 + 7 = 23 (Quase o melhor caminho, pois o melhor é Jesus (João 14:6))

**C**) 6 + 2 + 1 + 9 + 8 = 26

**D**) 4 + 2 + 1 + 12 + 8 = 27

**E**) 1 + 2 + 4 + 8 + 12 = 27

![](grupo-1-pergunta-3.png)

*Resposta:*

------

## Grupo 2 - Algoritmo de Kruskal

### 1. O que é uma AGM e quais suas aplicações no dia-a-dia? (cite 3 exemplos)

*Resposta:*

    É o menor caminho entre dois vértices conforme peso das arestas.

    Exemplos:

    - Melhor caminho no GPS (Waze, Google Maps, etc);
    - Cabeamento de redes de computador telefonia ou elétrica;
    - Rede neural (???)

### 2. Qual a AGM do grafo abaixo?

![](grupo-2-pergunta-2.png)

*Resposta:*

![](grupo-2-resposta-2.png)

### 3. Desenhe o grafo descrito na tabela e execute o algoritmo de kruskal no mesmo, colocando as arestas ordenadas.

![](grupo-2-pergunta-3.png)

*Resposta:*

![](grupo-2-resposta-3-grafo-original.png)

![](grupo-2-resposta-3-grafo-kruskal.png)

------

## Grupo 3 - Algoritmo A*

### 1. Qual é o tipo de busca que o algoritmo A* utiliza?

*Resposta:*

    Busca em largura, por que ele visita os vizinhos para adicionar a uma lista que chamamos de "lista aberta"

### 2. Explique as variáveis da fórmula para encontrar o custo F?

*Resposta:*

    G = custo de movimento até um determinado ponto
    H = é o custo direto de movimento até o nó final
    F = soma do G e do H

### 1. Cite uma utilidade do algoritmo e dê um exemplo.

*Resposta:*

    Encontrar o caminho mais efetivo entre 2 pontos, exemplo de uma rota do Waze;

------

## Grupo 4 - Algoritmo de Prim

### 1. O algoritmo de Prim sucedeu qual outro?

*Resposta:*

    Kruskal


### 2. Qual a semelhança com o algoritmo de árvore aleatória? (decision random tree)

*Resposta:*

    Buscam o melhor caminho mas o randômico (de prim) não tem nenhum critério, não considera o peso das arestas

### 3. Quais implementações podem ser feitas ou utilizadas com este algoritmo?

*Resposta:*

    Uma fazendo com um ponto de rede em cada pivô de irrigação, querendo economizar cabo garantindo boa qualidade de sinal.

------

## Grupo 5 - Algoritmo contador de componentes

### 1. Por que tanto a busca em profundidade ou em largura funciona para esse algoritmo?

*Resposta:*

    Por que todos os vértices deverão ser visitados.
    O que aumenta são a quantidade de buscas enquanto há vértice ainda não visitado.

### 2. Qual a definição de um componente?

*Resposta:*

    É o subgrafo induzido por qualquer subconjunto fechado do seu conjunto de vértices. Sendo assim, qualquer componente de um grafo é subgrafo conexo.

### 3. Como descobrir a quantidade de componentes de um grafo?

*Resposta:*

    Através do número de buscas feitas no grafo, pois o número de buscas será a quantidade de componentes. Ao término de cada busca é verificado se ainda existe vértice não visitado, então é feito uma nova busca, até que todos os vértices tenham sido visitados.

------

## Grupo 6 - Algoritmo da ordenação dos pesos das arestas

### 1. Como resolver um algoritmo por ordenação do peso das arestas?

*Resposta:*

### 2. Cinco localidades, que designaremos pelas letras A, B, C, D, E, estão ligadas entre si por diversas estradas. As estradas existentes e as respectivas distâncias são: 
[AB] 7km, [AE] 17km, [BC] 10Km , [BD] 21Km, [CE] 8Km, [CD] 12 Km, [ED] 33Km, [EB] 11Km.

*Resposta:*

### 3. Este grafo admite um circuito hamiltoniano? Caso sim, descreva a sequência de vértices de um possível circuito hamiltoniano.

![](grupo-6-pergunta-3.png)

*Resposta:*


------

## Grupo 7 - Algoritmo de Floyd-Warshall

### 1. O Algoritmo de Floyd-Warshall foi explicado no mesmo ano em que foi publicado?

*Resposta:*

    Foi publicado e reconhecido na forma atual por Robert Floyd em 1962, Mas foi essencialmente o mesmo algoritmo que foi anteriormente publicado por Bernard Roy em 1959, e também por Stephen Warshall em 1962 por ter encontrado um fechamento transitivo do grafo e relacionado ao algoritmo de Kleene (publicado em 1956).
    A formula moderna do algoritmo como 3 loops acoplados foi primeiro descrito NO MESMO ANO em 1962 por Peter Ingerman.
    fonte: https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm

### 2. Dado o grafo abaixo, utilize o algoritmo de Floyd-Warshall para calcular qual seria o peso e a ordem das arestas do menor caminho entre o vértice 1 e o vértice 2.

![](grupo-7-pergunta-2.png)

*Resposta:*

    Headeche

### 3. Cite 3 exemplos onde podemos utilizar o algoritmo de Floyd-Warshall. 

*Resposta:*

- Encontrar a rota mais econômica de um ônibus circular, que tem os pontos específicos pra passar, e um caminho de partida e chegada específico
- O percurso que gasta menos energia entre os locais do mapa do jogo de celular "Last Day on Earth, Survival"
- Qualquer situação onde precisa de mais velocidade em um percurso
